require 'swagger_helper'

describe 'People API' do

  path '/people' do

    post 'Creates a Person' do
      tags 'People'
      consumes 'application/json'
      parameter name: :person, in: :body, schema: {
        type: :object,
        properties: {
          first_name: { type: :string },
          last_name: { type: :string },
          date_of_birth: { type: :string },
          nationality: { type: :string }
        },
        required: [ 'first_name', 'last_name', 'date_of_birth' ]
      }
      parameter({
        in: :header,
        type: :string,
        name: :Authorization,
        required: true,
        description: 'User Token'
      })

      before { User.create(name: 'Dummy User', email: 'dummy_user@myemail.com', password: '12345678') }

      response '200', 'person created' do
        let(:Authorization) { AuthenticateUser.call('dummy_user@myemail.com', '12345678').result }
        let(:person) do 
          { 
            first_name: 'Owen', 
            last_name: 'wilson', 
            nationality: 'American',
            date_of_birth: '1969/11/25'
          }
        end
        run_test!
      end
    end
  end

  path '/people' do

    get 'Retrieves people' do
      tags 'People'
      produces 'application/json'
      parameter name: :by, :in => :query, :type => :string
      parameter name: :name, :in => :query, :type => :string

      response '200', 'person found' do
        schema type: :object, properties: {}
        let(:by) { 'name' }
        let(:name) { 'Denzel' }
        before do
          Person.create(first_name: 'Denzel', last_name: 'Washington', date_of_birth: '1965/05/01', nationality: 'American')
          Person.create(first_name: 'Jessica', last_name: 'Alba', date_of_birth: '1983/02/01', nationality: 'American')
        end
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['data'][0]['attributes']['first_name']).to eq('Denzel')
        end
      end
    end
  end
end