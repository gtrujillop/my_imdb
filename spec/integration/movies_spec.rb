require 'swagger_helper'

describe 'Movies API' do

  path '/movies' do

    post 'Creates a Movie' do
      tags 'Movie'
      consumes 'application/json'
      parameter name: :movie, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          release_date: { type: :string },
        },
        required: [ 'name', 'release_date']
      }
      parameter({
        in: :header,
        type: :string,
        name: :Authorization,
        required: true,
        description: 'User Token'
      })

      before { User.create(name: 'Dummy User', email: 'dummy_user@myemail.com', password: '12345678') }

      response '200', 'movie created' do
        let(:Authorization) { AuthenticateUser.call('dummy_user@myemail.com', '12345678').result }
        let(:movie) do 
          { 
            name: 'Saving Private Ryan', 
            release_date: '1998/04/05'
          }
        end
        run_test!
      end
    end
  end

  path '/movies' do

    get 'Retrieves movies' do
      tags 'Movie'
      produces 'application/json'
      parameter name: :by, :in => :query, :type => :string
      parameter name: :name, :in => :query, :type => :string

      response '200', 'movie found' do
        schema type: :object, properties: {}
        let(:by) { 'name' }
        let(:name) { 'Rambo' }
        before do
          Movie.create(name: 'Rambo 1', release_date: '1978/03/21')
          Movie.create(name: 'Rambo 5', release_date: '2008/07/15')
        end
        run_test! do |response|
          data = JSON.parse(response.body)
          expect(data['data'][0]['attributes']['name']).to eq('Rambo 1')
          expect(data['data'][1]['attributes']['name']).to eq('Rambo 5')
        end
      end
    end
  end
end