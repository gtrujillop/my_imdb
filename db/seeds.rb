# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u1 = User.create(name: 'Dummy User', email: 'dummy_user@myemail.com', password: '12345678')

p1 = Person.create(first_name: 'Denzel', last_name: 'Washington', date_of_birth: '1965/05/01', nationality: 'American')
p2 = Person.create(first_name: 'Sylvester', last_name: 'Stallone', date_of_birth: '1945/05/01', nationality: 'American')
p3 = Person.create(first_name: 'Woody', last_name: 'Allen', date_of_birth: '1945/12/01', nationality: 'American')
p4 = Person.create(first_name: 'Meryl', last_name: 'Streep', date_of_birth: '1965/05/01', nationality: 'American')

a1 = Alias.create(name: 'Rocky Balboa', person: p2)
a2 = Alias.create(name: 'Ely', person: p1)

m1 = Movie.create(name: 'The Book of Ely', release_date: '2010/06/05')
m2 = Movie.create(name: 'Midnight in Paris', release_date: '2012/10/24')
m3 = Movie.create(name: 'Devil Wears Prada', release_date: '2009/03/21')
m4 = Movie.create(name: 'Rambo 1', release_date: '1978/03/21')
m5 = Movie.create(name: 'Rambo 5', release_date: '2008/07/15')

r1 = Role.create(role_type: 'actor', movie: m1, person: p1)
r2 = Role.create(role_type: 'director', movie: m2, person: p3)
r3 = Role.create(role_type: 'actress', movie: m3, person: p4)
r4 = Role.create(role_type: 'actor', movie: m4, person: p2)
r5 = Role.create(role_type: 'actor', movie: m5, person: p2)
r6 = Role.create(role_type: 'director', movie: m5, person: p2)
r7 = Role.create(role_type: 'producer', movie: m5, person: p2)