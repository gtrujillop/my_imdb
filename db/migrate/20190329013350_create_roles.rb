class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.integer :role_type, null: false, default: 0
      t.references :person, index: true, foreign_key: true
      t.references :movie, index: true, foreign_key: true

      t.timestamps
    end
    add_index :roles, [:person_id, :movie_id, :role_type], unique: true
  end
end
