class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :name, null: false
      t.text :sinopsis
      t.date :release_date, null: false

      t.timestamps
    end
    add_index :movies, :name
    add_index :movies, :release_date
  end
end
