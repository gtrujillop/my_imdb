# MyIMDB API

This is a small JSON API that provides a small and lightweight way to manage a Movie Database. This includes the following endpoints:

  - POST to /authenticate
  - GET/POST/DELETE/PATCH to /movies
  - GET/POST/DELETE/PATCH to /people
  
# Libraries

  In order to create a lightweight API, we're using a only-api Rails 5.2.3 application.
  
    - including JWT and Bcrypt for User Authentication by using a Token. 
    - For storage, using pg and searchkick (planning to use Elasticsearch in the future to improve search and analytics).
    - For fast JSON serialization, the gem json-api is being used
    - For API docs, the gem rswag along with rspec are being used to build a JSON API compliant documentation + integration testing.
    - For CORS, using the rack-cors gem
    - For Background Jobs, sidekiq (not implemented)

### Production Access

  - https://my-imdb-getp.herokuapp.com/
  - https://my-imdb-getp.herokuapp.com/movies?by=name&name=Paris
  - https://my-imdb-getp.herokuapp.com/people?by=name&name=Denzel
  - https://my-imdb-getp.herokuapp.com/people?by=role&name=producer
  - https://my-imdb-getp.herokuapp.com/people?by=role&name=actress
  
### API Docs

  https://my-imdb-getp.herokuapp.com/api-docs/index.html


### To create/delete/update new people or movies, must be authenticated with a valid user
  - A valid user in production:
    - POST to /authenticate
    - JSON request body should contain: email: 'dummy_user@myemail.com' password: '12345678'
    - Response should include API token
    - POST to i.e. https://my-imdb-getp.herokuapp.com/movies including the API token in the Authorization header 