# == Schema Information
#
# Table name: roles
#
#  id         :bigint(8)        not null, primary key
#  role_type  :integer          default("actor"), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  movie_id   :bigint(8)
#  person_id  :bigint(8)
#
# Indexes
#
#  index_roles_on_movie_id                              (movie_id)
#  index_roles_on_person_id                             (person_id)
#  index_roles_on_person_id_and_movie_id_and_role_type  (person_id,movie_id,role_type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (movie_id => movies.id)
#  fk_rails_...  (person_id => people.id)
#

require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
