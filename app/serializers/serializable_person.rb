class SerializablePerson < JSONAPI::Serializable::Resource
  type 'people'
  attributes :first_name, :last_name, :date_of_birth, :nationality

  has_many :roles do
    meta do
      { count: @object.roles.count }
    end
  end

end
