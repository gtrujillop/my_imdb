class SerializableMovie < JSONAPI::Serializable::Resource
  type 'movies'
  attributes :name, :release_date

  attribute :release_year do
    @object.to_roman(@object.release_date.year)
  end

  has_many :roles
end
