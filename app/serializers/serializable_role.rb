class SerializableRole < JSONAPI::Serializable::Resource
  type 'roles'
  attributes :role_type, :movie_name, :person_id, :movie_id

  attribute :person_full_name do
    @object.person.full_name
  end

  belongs_to :person
  belongs_to :movie
end
