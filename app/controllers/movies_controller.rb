class MoviesController < ApplicationController
  before_action :authenticate_request, only: [:create, :update, :delete]
  before_action :search, only: :index

  def index
    render jsonapi: @movies,
           include: [:roles],
           fields: { 
             movies: [:name, :release_year],
             roles: [:role_type, :person_full_name, :movie_id]
           }
  end

  def delete
    @movie = Movie.find(movie_params[:id])
    if @movie
      @movie.destroy
      render json: 'Movie deleted successfully.'
    else
      render json: 'Movie not found', status: :not_found
    end
  rescue => e
    render json: "Errors while deleting movie, #{e.message}", status: :server_error
  end

  def create
    @movie = Movie.new(movie_params)
    if @movie.save
      render jsonapi: @movie
    else
      render json: "Could not create movie. Errors: #{@movie.errors.full_messages}", status: :unprocessable_entity
    end
  rescue => e
    render json: "Errors while creating movie, #{e.message}", status: :server_error
  end

  def update
    
  end

  private

  def movie_params
    params.require(:movie)
          .permit(:id, :name, :release_date, role_attributes: [:id, :role_type, :person_id, :movie_id])
  end

  def search
    unless params[:by]
      @movies = Movie.includes(roles: :person).all
    else
      @movies = Movie.includes(roles: :person).by_name(params[:name]) if params[:by].downcase == 'name'
      @movies = Movie.includes(roles: :person).by_actor(params[:name]) if params[:by].downcase == 'actor'
    end
  end
  
end
