class PeopleController < ApplicationController
  before_action :authenticate_request, only: [:create, :update, :delete]
  before_action :search, only: :index

  def index
    render jsonapi: @people,
           include: [:roles],
           fields: { 
             people: [:first_name, :last_name, :date_of_birth, :nationality],
             roles: [:role_type, :movie_name, :person_id]
           }
  end

  def delete
    @person = Person.find(person_params[:id])
    if @person
      @person.destroy
      render json: 'Person deleted successfully.'
    else
      render json: 'Person not found', status: :not_found
    end
  rescue => e
    render json: "Errors while deleting person, #{e.message}", status: :server_error    
  end

  def create
    @person = Person.new(person_params)
    if @person.save
      render jsonapi: @person
    else
      render json: "Could not create person. Errors: #{@person.errors.full_messages}", status: :unprocessable_entity
    end
  rescue => e
    render json: "Errors while creating person, #{e.message}", status: :server_error
  end

  def update
    
  end

  private

  def person_params
    params.require(:person)
          .permit(
            :id, 
            :first_name, 
            :last_name, 
            :date_of_birth, 
            :nationality, 
            role_attributes: [:id, :role_type, :person_id, :movie_id]
          )
  end

  def search
    unless params[:by]
      @people = Person.includes(:aliases, roles: :movie).all
    else
      @people = Person.includes(:aliases, roles: :movie).by_name(params[:name]) if params[:by].downcase == 'name'
      @people = Person.includes(:aliases, roles: :movie).by_movie(params[:name]) if params[:by].downcase == 'movie'
      @people = Person.includes(:aliases, roles: :movie).by_role(params[:name]) if params[:by.downcase] == 'role'
    end
  end
  
end
