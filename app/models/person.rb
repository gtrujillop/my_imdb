# == Schema Information
#
# Table name: people
#
#  id            :bigint(8)        not null, primary key
#  date_of_birth :date             not null
#  first_name    :string           not null
#  last_name     :string           not null
#  nationality   :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Person < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name, presence: true

  has_many :roles
  has_many :aliases
  
  accepts_nested_attributes_for :roles, allow_destroy: true

  scope :by_name, -> (name) { 
    where(
      "CONCAT(LOWER(people.first_name),' ',LOWER(people.last_name)) LIKE ?", "%#{name.downcase}%"
    ) if name 
  }
   scope :by_nationality, -> (name) { 
    where(
      "LOWER(people.nationality) LIKE ?", "%#{name.downcase}%"
    ) if name 
  }
  scope :by_movie, -> (name) { 
    joins(roles: :movie)
    .where(
      "LOWER(movies.name) LIKE ?", "%#{name.downcase}%"
    ) if name 
  }
  scope :by_role, -> (name) { 
    joins(:roles)
    .where(
      "roles.role_type = ?", Role.role_types[name.downcase]
    ) if name 
  }

  def full_name
    "#{first_name} #{last_name}" 
  end
end
