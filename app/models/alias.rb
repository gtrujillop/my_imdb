# == Schema Information
#
# Table name: aliases
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  person_id  :bigint(8)
#
# Indexes
#
#  index_aliases_on_person_id  (person_id)
#
# Foreign Keys
#
#  fk_rails_...  (person_id => people.id)
#

class Alias < ApplicationRecord
  validates :name, presence: true
  
  belongs_to :person
end
