module YearHelper
  ROMANS = {  
    1000 => "M",
    900 => "CM",
    500 => "D",
    400 => "CD",
    100 => "C",
    90 => "XC",
    50 => "L",
    40 => "XL",
    10 => "X",
    9 => "IX",
    5 => "V",
    4 => "IV",
    1 => "I" 
  }
  def to_roman(year)
    raise TypeError.new('year must be a Fixnum') unless year.is_a?(Fixnum)
    # We start with an empty string and divide by the largest roman numeral possible in the hash
    # Then we keep breaking down the remainder until it is 0
    ROMANS.reduce("") do | memo, (arabic, roman) |
      quotient, year = year.divmod(arabic)
      # We multiply the quotient by the key's value (e.g. "I" * 3 = "III") and push it to the end of the string
      memo << roman * quotient
    end
  end
end
