# == Schema Information
#
# Table name: movies
#
#  id           :bigint(8)        not null, primary key
#  name         :string           not null
#  release_date :date             not null
#  sinopsis     :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_movies_on_name          (name)
#  index_movies_on_release_date  (release_date)
#

class Movie < ApplicationRecord
  include YearHelper
  
  validates :name, presence: true
  validates :release_date, presence: true
  
  has_many :roles

  accepts_nested_attributes_for :roles, allow_destroy: true

  scope :by_name, -> (name) { where('LOWER(name) LIKE ?', "%#{name.downcase}%") if name }
  scope :by_actor, -> (name) { 
    joins(roles: :person)
    .where(
      "CONCAT(LOWER(people.first_name),' ',LOWER(people.last_name)) LIKE ?", "%#{name.downcase}%"
    ) if name 
  }

end
